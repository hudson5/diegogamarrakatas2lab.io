// comece a criar a sua função add na linha abaixo
//FUNCTION ADD
function add(a, b){
    return a + b
}
console.log('ADD FUNCTION')
console.log(add(3, 5))

// descomente a linha seguinte para testar sua função
console.assert(add(3, 5) === 8, 'A função add não está funcionando como esperado');

// comece a criar a sua função multiply na linha abaixo
function multiply(m1, m2){
    let result = 0
    if(m2 % 2 === 0){
        for(let i = 1; i <= m2; i += 2){
        result = result + add(m1, m1)
    }
}else{
    for(let i = 1; i <= m2; i += 2){
        result = result + add(m1, m1)
    }result = result - m1

}
    return result
}

console.log('MULTIPLY FUNCTION')
console.log(multiply(4, 9))

// descomente a linha seguinte para testar sua função
console.assert(multiply(4, 6) === 24, 'A função multiply não está funcionando como esperado');


// comece a criar a sua função power na linha abaixo
function power(n, x){
    let result = 1
        for(let i = 1; i <= x; i += 2){
        result = result * multiply(n, n)
    }
    return result
}

console.log('POWER FUNCTION')
console.log(power(3, 5))

// descomente a linha seguinte para testar sua função
console.assert(power(3, 4) === 81, 'A função power não está funcionando como esperado');


// comece a criar a sua função factorial na linha abaixo
function factorial(f){
    let result = 1
    for(let i = 1; i <= f; i++){
        result = multiply(i, result)
    }
    return result
}

console.log('FACTORIAL FUNCTION')
console.log(factorial(5))
// descomente a linha seguinte para testar sua função
console.assert(factorial(5) === 120, 'A função factorial não está funcionando como esperado');


/**
 * BONUS (aviso: o grau de dificuldade é bem maior !!!)
 */

// crie a função fibonacci


// descomente a linha seguinte para testar sua função
// console.assert(fibonacci(8) === 13, 'A função fibonacci não está funcionando como esperado');
